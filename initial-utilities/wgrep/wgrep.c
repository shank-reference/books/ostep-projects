#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void grep(char *term, FILE *stream) {
    char *line;
    unsigned long n = 128;
    line = malloc(n);

    if (line == NULL) {
        printf("error in malloc\n");
        free(line);
        exit(1);
    }

    while (getline(&line, &n, stream) != -1) {
        if (strstr(line, term) != NULL) {
            // found, print the line
            printf("%s", line);
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("wgrep: searchterm [file ...]\n");
        exit(1);
    }

    if (argc == 2) {
        // stdin
        grep(argv[1], stdin);

    } else {
        // files
        for (int i = 2; i < argc; i++) {
            FILE *fp = fopen(argv[i], "r");
            if (fp == NULL) {
                printf("wgrep: cannot open file\n");
                exit(1);
            }

            grep(argv[1], fp);
        }
    }

    return 0;
}
