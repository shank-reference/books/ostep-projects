#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    // read a line (or some characters)
    // if it is eof, return 0
    // else print out the read characters onto stdout
    char buf[256];

    // no files specified
    if (argc == 1) {
        return 0;
    }

    for (int i = 1; i < argc; i++) {
        FILE *fp = fopen(argv[i], "r");
        if (fp == NULL) {
            printf("wcat: cannot open file\n");
            exit(1);
        }

        char *res;
        while ((res = fgets(buf, 256, fp)) != NULL) {
            printf("%s", buf);
        }
        fclose(fp);
    }

    return 0;
}
