#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

enum next { Count, Char };

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("wunzip: file1 [file2 ...]\n");
        exit(1);
    }

    int count;
    char ch;

    for (int i = 1; i < argc; i++) {
        FILE *fp = fopen(argv[i], "r");
        if (fp == NULL) {
            printf("wunzip: unable to open file\n");
            exit(1);
        }

        while (true) {
            if (fread(&count, sizeof(count), 1, fp) != 1) {
                break;
            }

            if (fread(&ch, sizeof(ch), 1, fp) != 1) {
                break;
            }

            for (int i = 0; i < count; i++) {
                printf("%c", ch);
            }
        }

        fclose(fp);
    }
}
