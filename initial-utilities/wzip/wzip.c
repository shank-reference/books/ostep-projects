#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("wzip: file1 [file2 ...]\n");
        exit(1);
    }

    char ch = '\0';
    char prev = '\0';
    int count = 0;

    for (int i = 1; i < argc; i++) {
        FILE *fp = fopen(argv[i], "r");
        if (fp == NULL) {
            printf("wzip: unable to open file\n");
            exit(1);
        }

        while ((ch = fgetc(fp)) != EOF) {
            if (count == 0) {
                prev = ch;
                count++;
            } else if (ch == prev) {
                count++;
            } else {
                fwrite(&count, sizeof(count), 1, stdout);
                fwrite(&prev, sizeof(prev), 1, stdout);
                prev = ch;
                count = 1;
            }
        }

        fclose(fp);
    }

    fwrite(&count, sizeof(count), 1, stdout);
    fwrite(&prev, sizeof(prev), 1, stdout);
}
